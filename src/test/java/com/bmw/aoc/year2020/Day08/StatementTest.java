package com.bmw.aoc.year2020.Day08;

import edu.princeton.cs.algs4.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class StatementTest {

    @BeforeAll
    static void setUp() {

        new Statement(Statement.Instruction.NOP, true, 0);
        new Statement(Statement.Instruction.ACC, true, 1);
        new Statement(Statement.Instruction.JMP, true, 4);
        new Statement(Statement.Instruction.ACC, true, 3);

        new Statement(Statement.Instruction.JMP, false, 3);
        new Statement(Statement.Instruction.ACC, false, 99);
        new Statement(Statement.Instruction.ACC, true, 1);
        new Statement(Statement.Instruction.JMP, false, 4);

        new Statement(Statement.Instruction.ACC, true, 6);

    }


    @Test
    void testCycleDetectionDirectedGraph() {

        Digraph programInstructionDiGraph = new Digraph(Statement.getStatements().size());

        Statement.getStatements().forEach(
            statement -> programInstructionDiGraph.addEdge(statement.getAddress(), statement.getAddressOfNextStatementRelativeToThisStatement()));

        System.out.println("programInstructionDiGraph = " + programInstructionDiGraph);

        BreadthFirstDirectedPaths breadthFirstDirectedPaths = new BreadthFirstDirectedPaths(programInstructionDiGraph, 0);

        Statement
            .getStatements()
            .stream()
            .filter(statement -> breadthFirstDirectedPaths.hasPathTo(statement.getAddress()))
            .filter(statement -> "ACC".equals(statement.instruction.toString()))
            .forEach(statement -> Statement.accumulator += statement.value);

        System.out.println("accumulator = " + Statement.accumulator);

    }

}