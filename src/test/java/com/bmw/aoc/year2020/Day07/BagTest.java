package com.bmw.aoc.year2020.Day07;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class BagTest {

    static Map<String, Bag> bagStore = new HashMap<>();

    private static final Logger LOGGER = Logger.getLogger(BagTest.class.getName());

/*


    @BeforeAll
    static void initAll() {

        bagStore.putIfAbsent("light red", new Bag("light red"));
        bagStore.putIfAbsent("dark orange", new Bag("dark orange"));
        bagStore.putIfAbsent("bright white", new Bag("bright white"));
        bagStore.putIfAbsent("muted yellow", new Bag("muted yellow"));
        bagStore.putIfAbsent("shiny gold", new Bag("shiny gold"));
        bagStore.putIfAbsent("dark olive", new Bag("dark olive"));
        bagStore.putIfAbsent("vibrant plum", new Bag("light red"));
        bagStore.putIfAbsent("faded blue", new Bag("faded blue"));
        bagStore.putIfAbsent("dotted black", new Bag("dotted black"));

        // light red
        bagStore.get("light red").add(bagStore.get("bright white")); // -> 1

        bagStore.get("light red").add(bagStore.get("muted yellow")); // -> 2
        bagStore.get("light red").add(bagStore.get("muted yellow")); // -> 2

        // dark orange
        bagStore.get("dark orange").add(bagStore.get("bright white")); // -> 1
        bagStore.get("dark orange").add(bagStore.get("bright white")); // -> 1
        bagStore.get("dark orange").add(bagStore.get("bright white")); // -> 1

        bagStore.get("dark orange").add(bagStore.get("muted yellow")); // -> 2
        bagStore.get("dark orange").add(bagStore.get("muted yellow")); // -> 2
        bagStore.get("dark orange").add(bagStore.get("muted yellow")); // -> 2
        bagStore.get("dark orange").add(bagStore.get("muted yellow")); // -> 2

        // bright white -> 1
        bagStore.get("bright white").add(bagStore.get("shiny gold")); // -> 1

        // muted yellow -> 2
        bagStore.get("muted yellow").add(bagStore.get("shiny gold")); // -> 1
        bagStore.get("muted yellow").add(bagStore.get("shiny gold")); // -> 1

        bagStore.get("muted yellow").add(bagStore.get("faded blue"));
        bagStore.get("muted yellow").add(bagStore.get("faded blue"));
        bagStore.get("muted yellow").add(bagStore.get("faded blue"));
        bagStore.get("muted yellow").add(bagStore.get("faded blue"));
        bagStore.get("muted yellow").add(bagStore.get("faded blue"));
        bagStore.get("muted yellow").add(bagStore.get("faded blue"));
        bagStore.get("muted yellow").add(bagStore.get("faded blue"));
        bagStore.get("muted yellow").add(bagStore.get("faded blue"));

    }
*/

    @Test
    @Disabled
    void getCountOfChildDistinct() {


        Integer count = 0;

        Stream<Bag> streamOfBags = bagStore.values().stream();

        var test2 = streamOfBags.filter(b -> b.containsBagOrChildrenContainBag("shiny gold")).count();
        System.out.println("test2 = " + test2);

    }

    @Test
    void buildFirstRowOfSample(){

        String lightRed = "light red";

        Bag lightRedBag = bagStore.computeIfAbsent(lightRed, s -> new Bag(lightRed));

        String brightWhite = "bright white";
        Bag brightWhiteBag= bagStore.computeIfAbsent( brightWhite, s -> new Bag(brightWhite));

        lightRedBag.addChild(brightWhiteBag, 1);

        bagStore.values().forEach(System.out::println);

    }


}