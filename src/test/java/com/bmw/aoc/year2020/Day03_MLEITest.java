package com.bmw.aoc.year2020;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

class Day03_MLEITest {

	String[] list = { "..##.......", "#...#...#..", ".#....#..#.", "..#.#...#.#", ".#...##..#.", "..#.##.....",
			".#.#.#....#", ".#........#", "#.##...#...", "#...##....#", ".#..#...#.#" };

	@Test
	void test1() {

		assertEquals(7, Day03_MLEI.part1Basic(Arrays.asList(list)));

	}

	@Test
	void test2for1() {

		assertEquals(2, Day03_MLEI.countTrees(Arrays.asList(list), 1, 1));

	}

	@Test
	void test2for2() {

		assertEquals(7, Day03_MLEI.countTrees(Arrays.asList(list), 3, 1));

	}

	@Test
	void test2for3() {

		assertEquals(3, Day03_MLEI.countTrees(Arrays.asList(list), 5, 1));

	}

	@Test
	void test2for4() {

		assertEquals(4, Day03_MLEI.countTrees(Arrays.asList(list), 7, 1));

	}

	@Test
	void test2for5() {

		assertEquals(2, Day03_MLEI.countTrees(Arrays.asList(list), 1, 2));

	}

	@Test
	void test2() {

		assertEquals(336, Day03_MLEI.part2Basic(Arrays.asList(list)));

	}

}
