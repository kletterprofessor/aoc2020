package com.bmw.aoc.year2015;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

public class PassportValidationTest {

    /**
     * byr (Birth Year) - four digits; at least 1920 and at most 2002.
     */
    @Test
    public void testHeight(){

        String passportBirthYearSampleValid = "59in";

        // Pattern pattern = Pattern.compile("(\\d*)(\\w{2})");
        Pattern pattern = Pattern.compile("(\\d*)(cm|in)");
        Matcher matcher = pattern.matcher(passportBirthYearSampleValid);

        System.out.println(matcher.matches());

        System.out.println(matcher.groupCount());
        System.out.println(matcher.group(2));

        System.out.println(passportBirthYearSampleValid.matches("\\d*\\cm"));


    }

    @Test
    public void testHairColour(){

        List<String> hairColors = Arrays.asList("#123abc", "#123abz", "123abc");

        String testHairColor1 = "#123abc";

        Pattern pattern = Pattern.compile("#[0-9a-f]{6}");
        Matcher matcher = pattern.matcher(testHairColor1);

        assertTrue(matcher.matches());

    }

    public static boolean test (String s){
        Pattern pattern = Pattern.compile("\\d{2}");
        Matcher matcher = pattern.matcher(s);
        if (matcher.find()){
            return true;
        }
        return false;
    }


}
