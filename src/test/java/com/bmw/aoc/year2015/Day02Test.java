package com.bmw.aoc.year2015;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

class Day02Test {

	@Test
	void testGetDimensions() {
		assertEquals(Arrays.asList(2, 4, 6), Day02.getDimensions("2x4x6"));
//		fail("Not yet implemented");
	}

	@Test
	void testcalculateArea() {
		assertEquals(96, Day02.calculateArea(Arrays.asList(2, 4, 6)));
	}

	@Test
	void testcalculateRibbonLength() {
		assertEquals(14, Day02.calculateRibbonLength(Arrays.asList(1, 1, 10)));
		assertEquals(34, Day02.calculateRibbonLength(Arrays.asList(2, 3, 4)));
	}

}
