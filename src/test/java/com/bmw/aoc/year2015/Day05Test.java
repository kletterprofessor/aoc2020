package com.bmw.aoc.year2015;

import com.bmw.aoc.year2020.Day05.Permutations;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Day05Test {


    @Test
    public void bitConverterTest(){

        // Sample:   F B F B B F F   R L R
        // Binary:   0 1 0 1 1 0 0   1 0 1
        //
        // Sample:     B F F F B B F RRR
        //           0 1 0 0 0 1 1 0
        //
        // Encoded row: RLR
        // Binary       101
        //

    }

    @Test
    public void testRowEncodings(){

        assertTrue(decodeRow("FBFBBFFRLR") == 44);
        assertTrue(decodeRow("BFFFBBFRRR") == 70);
        assertTrue(decodeRow("FFFBBBFRRR") == 14);
        assertTrue(decodeRow("BBFFBBFRLL") == 102);

    }

    @Test
    public void testColumnEncodings(){

        assertTrue(decodeColumn("FBFBBFFRLR") == 5);
        assertTrue(decodeColumn("BFFFBBFRRR") == 7);
        assertTrue(decodeColumn("FFFBBBFRRR") == 7);
        assertTrue(decodeColumn("BBFFBBFRLL") == 4);
    }

    @Test
    public void testSeatEncodings(){

        assertTrue(decodeSeat("FBFBBFFRLR") == 357);
        assertTrue(decodeSeat("BFFFBBFRRR") == 567);
        assertTrue(decodeSeat("FFFBBBFRRR") == 119);
    }


    @Test
    public void testRowColSeatEncoding(){


        assertEquals("FBFBBFF", encodeRow(357) );
        assertEquals("BFFFBBF", encodeRow(567) );
        assertEquals("FFFBBBF", encodeRow(119) );

        assertEquals("RLR", encodeCol(357) );
        assertEquals("RRR", encodeCol(567) );
        assertEquals("RRR", encodeCol(119) );

        assertEquals("FBFBBFFRLR", encodeSeat(357) );
        assertEquals("BFFFBBFRRR", encodeSeat(567) );
        assertEquals("FFFBBBFRRR", encodeSeat(119) );

    }

    @Test
    public void testPermutations() {

        Set<String> setOfColums = new HashSet<>();

        List<String> rows =
            Stream.of("F", "R")
                .flatMap(s -> Stream.of(s + "R", s + "F"))
                .flatMap(s -> Stream.of(s + "F", s + "R"))
                .collect(Collectors.toList());

        // rows.forEach(System.out::println);

    }

    Integer decodeSeat(String encodedRowColumnSeatString){

        return decodeRow(encodedRowColumnSeatString)
            * 8
            + decodeColumn(encodedRowColumnSeatString);
    }

    Integer decodeRow(String encodedRowSeatString){

        String replacedString =
            encodedRowSeatString
                .replace("F", "0")
                .replace("B", "1");

        String rowString = "0"+ replacedString.substring(0, 7);

        return Integer.parseInt(rowString,2);

    }

    Integer decodeColumn(String encodedRowSeatString ){

        String replacedString =
            encodedRowSeatString
                .replace("R", "1")
                .replace("L", "0");

        String rowString = replacedString.substring(7, 10);

        return Integer.parseInt(rowString,2);
    }

    /**
     *
     * @param seatNumber decimal number of seat
     * @return encodedString of seat out of seat number
     * @link https://www.rapidtables.com/convert/number/decimal-to-hex.html
     */

    String encodeSeat(Integer seatNumber){

        String encodedRow = encodeRow(seatNumber);
        String encodedCol = encodeCol(seatNumber);
        String encodedSeat = encodedRow + encodedCol;

        System.out.println("seat: " + seatNumber + " row: " + encodedRow + " col: " + encodedCol + " encodedSeat: " + encodedSeat);

        return encodedSeat;
    }

    String encodeRow(Integer seat){

        String tenBitRowString = String.format("%010d", new BigInteger(Integer.toBinaryString(seat)));
        return tenBitRowString.substring(0, 7).replace("0", "F").replace("1", "B");

    }

    String encodeCol(Integer seat){

        String tenBitRowString = String.format("%010d", new BigInteger(Integer.toBinaryString(seat)));
        return tenBitRowString.substring(7, 10).replace("0", "L").replace("1", "R");

    }

}
