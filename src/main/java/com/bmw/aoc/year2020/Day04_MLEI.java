package com.bmw.aoc.year2020;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.bmw.aoc.Utils;

public class Day04_MLEI {

	public static void main(String[] args) throws Exception {
		Day04_MLEI kata = new Day04_MLEI();
		List<String> input = Utils.readInput(4, 2020);
		System.out.println(part1Basic(input));
//		System.out.println(part1(input));
		// System.out.println(part2Basic(input));
	}

	public static String part1Basic(List<String> instructions) {

		List<String> passportList = Day04_MLEI.readPassport(instructions);

		int valid = 0;

		for (int i = 0; i < passportList.size(); i++) {
			String passport = passportList.get(i).trim();
			List<String> attributesList = Arrays.asList(passport.split(" "));
			Map<String, String> attributes = new HashMap<>();

			for (int z = 0; z < attributesList.size(); z++) {
				String[] mapList = attributesList.get(z).split(":");
				attributes.put(mapList[0], mapList[1]);
			}
			if (checkPassport(attributes) >= 7) {
				valid++;
			}

		}

		return "part1 done: " + valid;

	}

//	public static String part1(List<String> instructions) {
//
//		List<String> passportList = Day04_MLEI.readPassport(instructions);
//
//		List<Map<String, String>> result = passportList.stream()
//				.map(passport -> Arrays.asList(passport.trim().split(" ")).stream()
//						.map(attribute -> attribute.split(":"))
//						.collect(Collectors.toMap(attribute -> attribute[0], attribute -> attribute[1]))
//				).collect(Collectors.toList());
//		
//		result.stream()
//
//		long result = passportList.stream().filter(attribute -> checkPassport(attribute) >= 7).count();
//
//		System.out.println(passportList);
//
//		return "part1 done: " + result;
//
//	}

	public static long part2Basic(List<String> instructions) {

		return 0;
	}

	public static List<String> readPassport(List<String> input) {

		List<String> passports = Stream.of(input.toString().replace("[", "").replace("]", "").split(", ,"))
				.map(x -> new String(x.replaceAll(",", ""))).collect(Collectors.toList());

		return passports;
	}

	public static Map<String, String> getPassportAttributes(List<String> passportInformation) {

		Map<String, String> passportAttributes = passportInformation.stream().map(attribute -> attribute.split(":"))
				.collect(Collectors.toMap(attribute -> attribute[0], attribute -> attribute[1]));

		System.out.println(passportAttributes);

		return passportAttributes;
	}

	public static int checkPassport(Map<String, String> map) {

		boolean hcl = false;
		boolean byr = false;
		boolean iyr = false;
		boolean eyr = false;
		boolean hgt = false;
		boolean ecl = false;
		boolean pid = false;
		boolean cid = false;
		int count = 0;

		if (map.containsKey("hcl")) {
			hcl = true;
			count++;
		}
		if (map.containsKey("byr")) {
			byr = true;
			count++;
		}

		if (map.containsKey("iyr")) {
			iyr = true;
			count++;
		}

		if (map.containsKey("eyr")) {
			eyr = true;
			count++;
		}

		if (map.containsKey("hgt")) {
			hgt = true;
			count++;
		}

		if (map.containsKey("ecl")) {
			ecl = true;
			count++;
		}

		if (map.containsKey("pid")) {
			pid = true;
			count++;
		}

//		if (map.containsKey("cid")) {
//			cid = true;
//			count++;
//		}

		return count;
	}

}
