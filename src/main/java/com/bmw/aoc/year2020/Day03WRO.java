package com.bmw.aoc.year2020;

import com.bmw.aoc.Utils;

import java.awt.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day03WRO {

    public static void main(String[] args) {

        System.out.println("AoC 20120 Day 3");

        List<String> input = Utils.readInputWRO(3, 2020);

        List<String> layers = input.subList(1, input.size());

        System.out.println("foundTrees " + SearchState.treeCounter);

        int treeCountPart2Test1 = getTreeCount(layers, 1, 1);
        int treeCountPart2Test2 = getTreeCount(layers, 3, 1);
        int treeCountPart2Test3 = getTreeCount(layers, 5, 1);
        int treeCountPart2Test4 = getTreeCount(layers, 7, 1);

        var subList =  IntStream.range(0, input.size())
            .filter(n -> n % 2 == 0)
            .mapToObj(input::get)
            .collect(Collectors.toList());

        int treeCountPart2Test5 = getTreeCount(subList, 1, 1);

        System.out.println("Test 1 Part 2: " + treeCountPart2Test1);
        System.out.println("Test 2 Part 2: " + treeCountPart2Test2);
        System.out.println("Test 3 Part 2: " + treeCountPart2Test3);
        System.out.println("Test 4 Part 2: " + treeCountPart2Test4);
        System.out.println("Test 5 Part 2: " + treeCountPart2Test5);

    }

    static int getTreeCount(List<String> layers, int slopeStepsRight, int slopeStepsDown){

        SearchState.initialize(slopeStepsRight, slopeStepsDown);

        layers
            .stream()
            .map(SearchState::new)
            .forEach(new Consumer<>() {
                @Override
                public void accept(SearchState searchState) {


                    if (searchState.isTreeAtCurrentHorizontalPosition()) {
                        searchState.incTreeCounter();
                    }

                    System.out.println(
                        "("
                            + searchState.getLayerPosition()
                            + ", "
                            + searchState.getHorizontalPosition()
                            + ")"
                            + " : "
                            + "[" + searchState.getStringOfCurrentLayer() + "]"
                            + "  char : " + searchState.getCharAtCurrentHorizontalPosition()
                            + "  isTree: " + searchState.isTreeAtCurrentHorizontalPosition()
                            + "  binIsTree: " + searchState.binaryIsTreeAdHorizontalPosition()
                            + "  treeCounter: " + searchState.getTreeCounter()
                            + "  genomicMultiplyFactor: " + searchState.getGenomicMultiplyFactor()
                            + "  genomicMutatedLayer: " + searchState.getGenomicMutatedLayer()
                    );
                }
            });

        return SearchState.treeCounter;

    }

    static public class SearchState {

        final static int SLOPE_STEP_RIGHT = 3;
        final static int SLOPE_STEP_DOWN = 1;

        // These aren't the only trees, though;
        // due to something you read about once involving arboreal genetics and biome stability,
        // the same pattern repeats to the right many times

        final static int GENETIC_SEQUENCE_LENGTH = 31;

        static int horizontalPosition = 0;
        static int layerPosition = 0;

        static int slopeStepsRight = 0;
        static int slopeStepsDown = 0;

        static int treeCounter = 0;

        static String currentLayer;

        SearchState(String pCurrentLayer) {

            currentLayer = pCurrentLayer;

            moveDownward();
            moveRight();
        }

        static void initialize(int pSlopeStepsRight, int pSlopeStepsDown){

            slopeStepsRight = pSlopeStepsRight;
            slopeStepsDown = pSlopeStepsDown;

            horizontalPosition = 0;
            layerPosition = 0;

            treeCounter = 0;

        }

        void moveDownward() {
            layerPosition += slopeStepsDown;
        }

        void moveRight() {
            horizontalPosition += slopeStepsRight;
        }

        static void setHorizontalPosition(int pHorizontalPosition) {
            horizontalPosition = pHorizontalPosition;
        }

        int getHorizontalPosition() {
            return horizontalPosition;
        }

        int getGenomicMultiplyFactor(){
            return horizontalPosition / GENETIC_SEQUENCE_LENGTH;
        }

        String getStringOfCurrentLayer() {

            return currentLayer;
        }

        String getGenomicMutatedLayer(){
            return currentLayer.repeat(getGenomicMultiplyFactor() + 1);
        }

        int getLayerPosition() {
            return layerPosition;
        }

        char getCharAtCurrentHorizontalPosition() {

            return getGenomicMutatedLayer().charAt(getHorizontalPosition());
        }

        boolean isTreeAtCurrentHorizontalPosition() {
            return getCharAtCurrentHorizontalPosition() == '#';
        }

        int binaryIsTreeAdHorizontalPosition() {
            return isTreeAtCurrentHorizontalPosition() ? 1 : 0;
        }

        void incTreeCounter(){ treeCounter += 1; }

        int getTreeCounter() { return treeCounter;}

    }

}
