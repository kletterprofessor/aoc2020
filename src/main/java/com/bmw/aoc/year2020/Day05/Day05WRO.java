package com.bmw.aoc.year2020.Day05;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day05WRO {

    public static void main(String... args) {

        Path path = Paths.get("puzzles/2020/day05WRO.txt");

        Optional <Integer> maxSeat = getMaxSeat(path);      // result 850 in my case
        Optional <Integer> minSeat = getMinSeat((path));

        System.out.println("Max seat number: " + maxSeat); // 850
        System.out.println("Min seat number: " + minSeat); // 11

        String codeOfLastSeat = encodeSeat(850);
        String codeOfFirstSeat = encodeSeat(11);

        System.out.println("Code of max seat : " + codeOfLastSeat);
        System.out.println("Code of min seat : " + codeOfFirstSeat);

        Integer rowOfLastSeat = decodeRow(codeOfLastSeat);
        Integer rowOfFirstSeat = decodeRow(codeOfFirstSeat);

        Integer colOfLastSeat = decodeColumn(codeOfLastSeat);
        Integer colOfFirstSeat = decodeColumn(codeOfFirstSeat);

        System.out.println("Last seat row: " + rowOfLastSeat + " col: " + colOfLastSeat);
        System.out.println("First seat row: " + rowOfFirstSeat + " col: " + colOfFirstSeat);

        System.out.println("Code of first seat in last row: " + encodeSeat(848));

        //  Last row:
        //
        //  848 849 850 <- max seat
        //  LLL LLR LRL
        //  0   1   2   3   4   5   6   7        //  row 106 -> BBFBFBF
        //
        //  ==> Last row has only 3 seats out of 8 possible ==> 5 seats do not exist in last row
        //
        //  First row:
        //              11  12  13  14  15
        //              LRR RLR RLL LLR LLL
        //  0   1   2   3   4   5   6   7       //   row 1   -> FFFFFFB
        //
        // --> First row has 5 seats of of 8 possible ==> 3 seats do not exist in the first row
        //
        // ==> Overall 8 seats do not exist
        //


        // Generate List of Integers from 850 - 11
        //

        // Create map of possible seats
        // starting from seat 11 , row 1 to seat 850 row 106
        Map<String, Integer> boardingPassMap = new HashMap<>();

        IntStream.range(
            minSeat.get(), maxSeat.get() + 1)
                .forEach(i -> boardingPassMap.putIfAbsent(encodeSeat(i), i));

        System.out.println("Number of seats in aircraft :"  + boardingPassMap.size());
        System.out.println("Number of boarding passes out of list: " + getNumberOfBoardingPasses(path));


        try (Stream<String> lines = Files.lines(path);) {

            lines
                .forEach(s -> boardingPassMap.remove(s));

        } catch (
            IOException ioe) {
            ioe.printStackTrace();
        }

        System.out.println("Number of remaining boarding passes: " + boardingPassMap.size());
        boardingPassMap.forEach((s, integer) -> System.out.println("Boarding pass left -> code: " + s + " seat number: " + integer));

    }

    /**
     *
     * @param seatNumber decimal number of seat
     * @return encodedString of seat out of seat number
     * @link https://www.rapidtables.com/convert/number/decimal-to-hex.html
     */

    public static String encodeSeat(Integer seatNumber){

        String encodedRow = encodeRow(seatNumber);
        String encodedCol = encodeCol(seatNumber);
        String encodedSeat = encodedRow + encodedCol;

        System.out.println("seat: " + seatNumber + " row: " + encodedRow + " col: " + encodedCol + " encodedSeat: " + encodedSeat);

        return encodedSeat;
    }

    public static String encodeRow(Integer seat){

        String tenBitRowString = String.format("%010d", new BigInteger(Integer.toBinaryString(seat)));
        return tenBitRowString.substring(0, 7).replace("0", "F").replace("1", "B");

    }

    public static String encodeCol(Integer seat){

        String tenBitRowString = String.format("%010d", new BigInteger(Integer.toBinaryString(seat)));
        return tenBitRowString.substring(7, 10).replace("0", "L").replace("1", "R");

    }

    public static Long getMySeatNumber(Path path){

        Long numberOfBoardingPassports = -1L;

        try (Stream<String> lines = Files.lines(path);) {

            numberOfBoardingPassports =  lines
                .map(Day05WRO::decodeSeat)
                .count();

            System.out.println("Number of boarding passports: " + numberOfBoardingPassports);

        } catch (
            IOException ioe) {
            ioe.printStackTrace();
        }

        return -1L;

    }

    public static Optional<Integer> getMaxSeat(Path path){

        try (Stream<String> lines = Files.lines(path);) {

            return lines
                .map(Day05WRO::decodeSeat)
                .max(Integer::compareTo);

        } catch (
            IOException ioe) {
            ioe.printStackTrace();
        }
        return Optional.empty();
    }

    public static long getNumberOfBoardingPasses(Path path){

        try (Stream<String> lines = Files.lines(path);) {

            return lines
                .map(Day05WRO::decodeSeat)
                .count();

        } catch (
            IOException ioe) {
            ioe.printStackTrace();
        }

        return -1L;
    }

    public static Optional<Integer> getMinSeat(Path path){

        try (Stream<String> lines = Files.lines(path);) {

            return lines
                .map(Day05WRO::decodeSeat)
                .min(Integer::compareTo);

        } catch (
            IOException ioe) {
            ioe.printStackTrace();
        }
        return Optional.empty();
    }


    public static Stream<Integer> getSeatPermutations(String row) {

        return Stream.of("R", "L")
            .flatMap(s -> Stream.of(s + "L", s + "R"))
            .flatMap(s -> Stream.of(s + "R", s + "L"))
            .map(s -> row + s)
            .peek(s -> System.out.println(s))
            .map(s -> decodeSeat(s));
    }

    static public Integer decodeSeat(String encodedRowColumnSeatString) {

        return decodeRow(encodedRowColumnSeatString)
            * 8
            + decodeColumn(encodedRowColumnSeatString);
    }

    static private Integer decodeRow(String encodedRowSeatString) {

        String replacedString =
            encodedRowSeatString
                .replace("F", "0")
                .replace("B", "1");

        String rowString = "0" + replacedString.substring(0, 7);

        return Integer.parseInt(rowString, 2);

    }

    static private Integer decodeColumn(String encodedRowSeatString) {

        String replacedString =
            encodedRowSeatString
                .replace("R", "1")
                .replace("L", "0");

        String rowString = replacedString.substring(7, 10);

        return Integer.parseInt(rowString, 2);

    }

}
