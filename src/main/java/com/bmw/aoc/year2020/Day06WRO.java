package com.bmw.aoc.year2020;

import com.bmw.aoc.year2020.Day04.ChunkSpliterator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day06WRO {

    // questions yes/no a - z
    // separate in groups / store in a set of character
    // sum over sets

    public static void main(String... args) {

        Path path = Paths.get("puzzles/2020/day06WRO.txt");

//        int resultPart1Variant1 = part1Variant1(path);
//        System.out.println("resultPart1Variant1 = " + resultPart1Variant1);
//        
//        int resultPart1Variant2 = part1Variant2(path);
//        System.out.println("resultPart1Variant2 = " + resultPart1Variant2);
//        
//        long  resultPart1Variant3 = part1Variant3(path);
//        System.out.println("resultPart1Varian3 = " + resultPart1Variant3);
        
//          long resultPart2Variant1 = part2Variant1(path);
//          System.out.println("resultPart2Variant1 = " + resultPart2Variant1);

//            long resultPart2Variant2 = part2Variant2(path);
//            System.out.println("resultPart2Variant2 = " + resultPart2Variant2)
//
             long resultPart2Variant3 = part2Variant3(path);
                System.out.println("resultPart2Variant3 = " + resultPart2Variant3);

    }

    static long part2Variant3(Path path){

        long returnValue = -1L;

        Function<List<String>, Set<Character>> mapper =
            strings -> {

                var setOfPossibleAnswers =
                    IntStream
                        .rangeClosed(97, 122)
                        .mapToObj(e -> (char)e)
                        .collect(Collectors.toSet());

                strings.stream().map(
                    s -> s.chars()
                        .mapToObj(e -> (char) e)
                        .filter(character -> Character.isLetter(character))
                        .collect(Collectors.toSet()))
                    .collect(Collectors.toList())
                    .forEach(characters -> setOfPossibleAnswers.retainAll(characters));

                return setOfPossibleAnswers;
            };

        try (Stream<String> lines = Files.lines(path)) {

            returnValue =
                ChunkSpliterator.toChunks(
                    lines,
                    Pattern.compile("^\\s*$").asPredicate(),
                    false)
                    .map(mapper)
                    .mapToLong(Set::size)
                    .sum();

        } catch (IOException e) {
            e.printStackTrace();
        }


        return returnValue;

    }

    static long part2Variant2(Path path){

        long returnValue = -1L;

        Function<List<String>, List<Set<Character>>> mapper =
            strings -> {

                List<Set<Character>> listCharSet = new ArrayList<>();
                strings.stream().map(
                    s -> s.chars()
                        .mapToObj(e -> (char) e)
                        .filter(character -> Character.isLetter(character))
                        .collect(Collectors.toSet()))
                    .collect(Collectors.toList())
                    .forEach(characters -> listCharSet.add(characters));

                System.out.println("listCharSet = " + listCharSet);
                return listCharSet;
            };

        Function<List<Set<Character>>, Set<Character>> mapper2 =
            sets -> {

                var setOfPossibleAnswers =
                    IntStream
                        .rangeClosed(97, 122)
                        .mapToObj(e -> (char)e)
                        .collect(Collectors.toSet());

                System.out.println("setOfPossibleAnswers before = " + setOfPossibleAnswers);

                sets.forEach(characters -> setOfPossibleAnswers.retainAll(characters));

                System.out.println("setOfPossibleAnswers after = " + setOfPossibleAnswers);

                return setOfPossibleAnswers;

            };

        try (Stream<String> lines = Files.lines(path)) {

            returnValue =
            ChunkSpliterator.toChunks(
                lines,
                Pattern.compile("^\\s*$").asPredicate(),
                false)
                .map(mapper)
                .map(mapper2)
                .mapToLong(Set::size)
                .sum();

        } catch (IOException e) {
            e.printStackTrace();
        }


        return returnValue;

    }

    static long part2Variant1(Path path){

        long returnValue = -1;

        final int[] sum = {0};

        var setOfPossibleAnswers = 
        IntStream
            .rangeClosed(97, 122)
            .mapToObj(e -> (char)e)
            .collect(Collectors.toSet());

        System.out.println("setOfPossibleAnswers = " + setOfPossibleAnswers);

        Function<List<String>, List<Set<Character>>> mapper =
            strings -> {

                List<Set<Character>> listCharSet = new ArrayList<>();
                strings.stream().map(
                    s -> s.chars()
                        .mapToObj(e -> (char) e)
                        .filter(character -> Character.isLetter(character))
                        .collect(Collectors.toSet()))
                        .collect(Collectors.toList())
                        .forEach(characters -> listCharSet.add(characters));

                return listCharSet;
            };


        try (Stream<String> lines = Files.lines(path)) {

            //var test =
                ChunkSpliterator.toChunks(
                    lines,
                    Pattern.compile("^\\s*$").asPredicate(),
                    false)
                    .map(mapper)
                    .forEach(new Consumer<List<Set<Character>>>() {
                        @Override
                        public void accept(List<Set<Character>> sets) {
                            System.out.println("sets = " + sets + " size of sets: "  + sets.size());

                            final int[] personIndex = {0};
                            var setOfPossibleAnswers =
                                IntStream
                                    .rangeClosed(97, 122)
                                    .mapToObj(e -> (char)e)
                                    .collect(Collectors.toSet());

                            System.out.println("setOfPossibleAnswers start = " + setOfPossibleAnswers);

                            sets.forEach(new Consumer<Set<Character>>() {
                                @Override
                                public void accept(Set<Character> characters) {

                                    System.out.println("personIndex: " + personIndex[0]);
                                    personIndex[0]++;

                                    setOfPossibleAnswers.retainAll(characters);
/*
                                    Map<Character, List<Character>> result =
                                        characters.stream().collect(Collectors.groupingBy(Character::charValue));
                                    sum[0] = sum[0] +  result.size();
                                    System.out.println("result = " + result + "size of result: " + result.size());

                                    System.out.println("sum = " + sum[0]);*/
                                }
                            });

                            System.out.println("setOfPossibleAnswers after = " + setOfPossibleAnswers.stream().distinct().collect(Collectors.toSet()));

                        }
                    });
                    //.forEach(System.out::println);

            // returnValue = test;
            // System.out.println(test);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnValue;


    }

    static long part1Variant3(Path path){

        long returnValue = -1;

        Function<List<String>, Set<Character>> mapper =
            strings -> {

                Set<Character> charSet = new HashSet<Character>();

                    strings.stream().map(
                    s -> s.chars()
                        .mapToObj(e -> (char) e)
                        .filter(character -> Character.isLetter(character))
                        .collect(Collectors.toSet()))
                        .peek(System.out::println)
                        .forEach(characters -> charSet.addAll(characters));

                System.out.println("charSet = " + charSet);

                return charSet;
            };

        try (Stream<String> lines = Files.lines(path)) {

            var test =
                ChunkSpliterator.toChunks(
                lines,
                Pattern.compile("^\\s*$").asPredicate(),
                false)
                .map(mapper)
                .mapToLong(Set::size)
                .sum();
                
            returnValue = test;
            
        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnValue;

    }
    
    static int part1Variant2(Path path){
        
        int returnValue = -1;

        List<Set<Character>> listOfGroups = new ArrayList<>();

        try (Stream<String> lines = Files.lines(path)) {

            ChunkSpliterator.toChunks(
                lines,
                Pattern.compile("^\\s*$").asPredicate(),
                false)
                .forEach(strings -> {
                    // get characters of each string
                    Set<Character> charSet = new HashSet<Character>();

                    strings.forEach(
                        s -> charSet.addAll(
                            s.chars().mapToObj(e -> (char)e)
                            .filter(character -> Character.isLetter(character))
                            .collect(Collectors.toSet())));

                    listOfGroups.add(charSet);
                });

            System.out.println(listOfGroups);

            var result = listOfGroups.stream().mapToInt(new ToIntFunction<Set<Character>>() {
                @Override
                public int applyAsInt(Set<Character> value) {
                    return value.size();
                }
            }).sum();

            returnValue = result;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnValue;

    }

    static int part1Variant1(Path path){

        int returnValue = -1;

        List<Set<Character>> listOfGroups = new ArrayList<>();

        try (Stream<String> lines = Files.lines(path)) {

            ChunkSpliterator.toChunks(
                lines,
                Pattern.compile("^\\s*$").asPredicate(),
                false)
                .forEach(strings -> {
                    // get characters of each string
                    Set<Character> charSet = new HashSet<Character>();

                    strings.forEach(new Consumer<String>() {
                        @Override
                        public void accept(String s) {

                            charSet.addAll(
                                s.chars()
                                    .mapToObj(e->(char)e)
                                    .filter(new Predicate<Character>() {
                                        @Override
                                        public boolean test(Character character) {
                                            return Character.isLetter(character);
                                        }
                                    })
                                    .collect(Collectors.toSet()));
                        }
                    });

                    listOfGroups.add(charSet);
                });

            System.out.println(listOfGroups);

            var result = listOfGroups.stream().mapToInt(new ToIntFunction<Set<Character>>() {
                @Override
                public int applyAsInt(Set<Character> value) {
                    return value.size();
                }
            }).sum();

            returnValue = result;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnValue;

    }

}
