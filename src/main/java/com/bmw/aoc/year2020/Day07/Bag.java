package com.bmw.aoc.year2020.Day07;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import java.util.logging.Logger;

import static java.lang.System.currentTimeMillis;

public class Bag extends ArrayList<Bag> {

    private static final Logger LOGGER = Logger.getLogger(Bag.class.getName());

    static Map <String, Bag> bagStore = new HashMap<>();

    static int highestIndexOfBag = 0;

    public static Integer numberOfBagsWithShinyGold = 0;

    public int index;

    final String name;

    public Integer getIndex(){
        return index;
    }

    public void setParent(Bag parent) {
        this.parent = parent;
    }

    public Bag getParent() {
        return parent;
    }

    Bag parent = null;

    Bag(final String name) {

        this.name = name;
        this.index = highestIndexOfBag;

        highestIndexOfBag += 1;

        LOGGER.fine("Created Bag: " + "id: " + index + " name: " + name);

    }

    public static Map<String, Bag> getBagStore() {
        return bagStore;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Bag){
            String toCompare = ((Bag) o).name;
            return name.equals(toCompare);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean contains(Object o) {

        Boolean b = super.contains(o);
        return b;
    }


    public void addChild(final Bag childBag, final Integer cardinality ){

        childBag.setParent(this);
        
        for(int i = 0; i < cardinality; i++){
            this.add(childBag);
        }
    }

    public Stream<Bag> getStreamOfChildren(){
        return this.stream();
    }

    public Stream<Bag> descendants() {
        return Stream.concat(
            Stream.of(this),
            getStreamOfChildren().flatMap(n -> n.descendants())
        );
    }

    public Boolean containsBagOrChildrenContainBag(final String bagName){

        Bag searchedBag = bagStore.get(bagName);

        Long start = currentTimeMillis();

        Long numberOfDescendants = this.descendants().filter(bag -> bag.contains(searchedBag)).count();

        Long end = currentTimeMillis();

        String parent = this.getParent() == null ? "null" : this.getParent().getName();

        LOGGER.info("Searching in bagName = " + this.getName() + ", " + " Parent: " + parent + " ," + " Descendants: " + numberOfDescendants + ", " + " Milliseconds needed: " + (end - start));

        if (
            numberOfDescendants > 0
        )
            return true;
        else
            return false;

    }

    List<Bag> getFlatListOfChildren(final Bag childBag){

        List<Bag> flatList = new ArrayList<>();

        if(!this.isEmpty()) {

            // add all from this bag...
            flatList.addAll(this.stream().filter(s -> contains(childBag)).collect(Collectors.toList()));

            // add childs
            this.stream().
                collect(Collectors.toList()).forEach(s ->
                flatList.addAll(s.getFlatListOfChildren(childBag)));

        }

        return flatList;
    }

    public Boolean hasBagOrChildrenHaveBag(final String childBagName){

        Bag childBag = bagStore.get(childBagName);

        return getFlatListOfChildren(childBag).stream().count() != 0 ? true :false;
    }


    @Override
    public String toString() {

        StringBuffer sb = new StringBuffer();

        String parent = this.parent == null ? "null" : this.parent.getName();

        sb.append("Bag: "
            + " id: " + this.index
            + " name: "  + this.getName()
            + " parent: " + parent);

        /*
        sb.append(" : childs [");

        this.stream().forEach( bag -> sb.append("name: " + bag.getName()).append(" id:  ").append(bag.getIndex()).append(", "));
        sb.append("]");

         */

        return sb.toString();
    }

}
