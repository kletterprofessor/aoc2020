package com.bmw.aoc.year2020.Day07;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Spliterator;
import java.util.function.Function;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import edu.princeton.cs.algs4.*;

import javax.lang.model.type.IntersectionType;

import static java.lang.System.currentTimeMillis;
import static java.lang.System.setOut;

public class Day07WRO {

    private static final Logger LOGGER = Logger.getLogger(Day07WRO.class.getName());

    public static void main(String... args) {

        LogManager manager = LogManager.getLogManager();
        try {

            InputStream stream = Day07WRO.class.getClassLoader().
                getResourceAsStream("logging.properties");

            manager.readConfiguration(stream);

        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }


        Path path = Paths.get("puzzles/2020/day07WRO.txt");

        Path path2 = Paths.get("puzzles/2020/day07part01Result.txt");

        try {

            Stream<String> lines = Files.lines(path);

            Spliterator<Bag> bagSpliterator = new BagSpliterator(lines);

            Stream<Bag> bags = StreamSupport.stream(bagSpliterator, false);

            var bagCount = bags.count();

            Bag shinyGold =  Bag.getBagStore().get("shiny gold");
            
            var test = shinyGold.descendants().count();
            System.out.println("test = " + test);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    static public Long part01(Collection<Bag> bagsOfBagStore){

        Digraph digraph = createDiGraph(bagsOfBagStore);

        Integer indexOfShinyGold = Bag.getBagStore().get("shiny gold").getIndex();

        var test = bagsOfBagStore.stream()
            .filter(bag -> !bag.getName().equals("shiny gold"))
            .map(bag -> {
                BreadthFirstDirectedPaths breadthFirstDirectedPaths = new BreadthFirstDirectedPaths(digraph, bag.getIndex());
                return breadthFirstDirectedPaths.hasPathTo(indexOfShinyGold);
            })
            .filter(aBoolean -> aBoolean)
            .count();

       return test;

    }


    public static Graph createGraph(Collection<Bag> bags){

        Graph graph = new Graph( bags.size());

        bags.forEach(b -> {

            for (Bag bag : b) {
                Bag realBag = Bag.getBagStore().get(b.getName());

                graph.addEdge(b.getIndex(), bag.getIndex());
            }

        });

        return graph;

    }

    public static Digraph createDiGraph(Collection<Bag> bags){

        Digraph diGraph = new Digraph( bags.size());

        bags.forEach(b -> {

            for (Bag bag : b) {
                Bag realBag = Bag.getBagStore().get(b.getName());

                diGraph.addEdge(b.getIndex(), bag.getIndex());
            }

        });

        return diGraph;

    }

    public static EdgeWeightedDigraph createEdgeWeightedDigrahp(Collection<Bag> bags){

        EdgeWeightedDigraph edgeWeightedDigraph = new EdgeWeightedDigraph(bags.size());

        bags.forEach(b -> {

            var child = b.stream().distinct();

            System.out.println("parent: " + b.getIndex() + " child = " + child.collect(Collectors.toList()));

        });


        return edgeWeightedDigraph;
    }

}
