package com.bmw.aoc.year2020.Day07;

import de.wro.learning.java8.advanced.spliterator.entity.Person;

import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Logger;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BagSpliterator implements Spliterator<Bag> {

    private static final Logger LOGGER = Logger.getLogger(BagSpliterator.class.getName());

    private final Spliterator<String> lineSpliterator;

    private List<String> decodedLines = null;

    public BagSpliterator(Stream<String> input) {

        this.lineSpliterator = input.spliterator();

    }

    public boolean tryAdvance(Consumer<? super Bag> action) {

        if(this.lineSpliterator.tryAdvance(s -> {

            List<String> strings = Arrays.asList(s);

            var test =
            strings.stream()
                .flatMap(str -> Pattern.compile("bags contain")
                    .splitAsStream(str))
                .collect(Collectors.toList());

            String parentBagName = test.get(0).trim();
            LOGGER.fine("parentName: " + parentBagName);
            Bag parentBag = Bag.getBagStore().computeIfAbsent(parentBagName, bag -> new Bag(parentBagName));

            String rawStringOfChildBags = test.get(1).trim();
            createAndAddDependentChildBags(parentBag, rawStringOfChildBags);

            action.accept(parentBag);

        }))
            return true;
        else
            return false;
    }

    Bag createAndAddDependentChildBags(Bag parent, String childInformation) {

        final String pattern = "(\\d)\\s(\\w*)\\s(\\w*)";

        Scanner scanner = new Scanner(childInformation);

        scanner.findAll(pattern).forEach(matchResult -> {

            String childBagName = matchResult.group(2) + " " + matchResult.group(3);
            Integer numberOfChildBagToBeAdded = Integer.parseInt(matchResult.group(1));

            LOGGER.fine("Parent bag : " +  "name:  " + parent.getName() + " id: " + parent.getIndex() +  " childToBeAdded: " + childBagName);

            Bag childBag = Bag.getBagStore().computeIfAbsent(childBagName, s -> new Bag(childBagName));

            parent.addChild(childBag, numberOfChildBagToBeAdded);
        });

        return parent;
    }

    @Override
    /**
     * Step 5
     * No parallelism  --> return null
     */
    public Spliterator<Bag> trySplit() {
        return null;
    }

    @Override
    /**
     * Step 4
     * As we know that 3 lines make a person we can divide the number which is returned by the
     * used line spliterator by 3
     */
    public long estimateSize() {
        return lineSpliterator.estimateSize();
    }

    @Override
    /**
     * Step 3
     * Just return characteristics of used line spliterator
     */
    public int characteristics() {
        return lineSpliterator.characteristics();
    }
}

