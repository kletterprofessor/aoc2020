package com.bmw.aoc.year2020;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.bmw.aoc.Utils;

import static java.lang.Integer.parseInt;

public class Day02WRO {


    public static void main(String[] args) {

        Pattern PASSWORD_POLICY_PATTERN = Pattern.compile("(\\d+)-(\\d+) (\\w): (\\w+)");

        System.out.println("AoC 20120 Day 2");

        List<String> input = Utils.readInputWRO(2, 2020);

        System.out.println(input);

        var v1 = input.stream()
            .map(line -> Utils.findRegexGroups(PASSWORD_POLICY_PATTERN, line))
            .map(groups -> PolicyAndPassword.fromGroups(groups.toArray(String[]::new)))
            .filter(policyAndPassword -> policyAndPassword.passwordValidator()).count();

        System.out.println(v1);

        var v2 = input.stream()
            .map(line -> Utils.findRegexGroups(PASSWORD_POLICY_PATTERN, line))
            .map(groups -> PolicyAndPassword.fromGroups(groups.toArray(String[]::new)))
            .filter(policyAndPassword -> policyAndPassword.isOfficialTobogganCorporateAuthenticationSystemValid()).count();

        System.out.println(v2);
    }


    static public class PolicyAndPassword{

        private final static Pattern PASSWORD_POLICY_PATTERN = Pattern.compile("(\\d+)-(\\d+) (\\w): (\\w+)");

        static int rangeStart = -1;
        static int rangeEnd = -1;

        static char letter;
        static String password;

        PolicyAndPassword(int rangeStart, int rangeEnd, char letter, String password){
            this.rangeStart = rangeStart;
            this.rangeEnd = rangeEnd;
            this.letter = letter;
            this.password = password;
        }

        static PolicyAndPassword fromGroups(String[] groups) {
            return new PolicyAndPassword(parseInt(groups[0]), parseInt(groups[1]), groups[2].charAt(0), groups[3]);
        }

        public boolean passwordValidator() {
            long count = password.chars()
                .filter(character -> character == letter)
                .count();
            return count >= rangeStart && count <= rangeEnd;
        }

        boolean isOfficialTobogganCorporateAuthenticationSystemValid() {
            return ((password.charAt(rangeStart - 1) == letter ? 1 : 0)
                + (password.charAt(rangeEnd - 1) == letter ? 1 : 0)) == 1;
        }

        @Override
        public String toString() {
            return "PolicyAndPassword{"
                + " range start: "+ Integer.toString(rangeStart)
                + " range end: " + Integer.toString(rangeEnd)
                + " letter: " + Character.toString(letter)
                + " password: " + password
                + "}";

        }
    }
}
