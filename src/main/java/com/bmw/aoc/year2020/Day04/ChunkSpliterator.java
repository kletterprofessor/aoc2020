package com.bmw.aoc.year2020.Day04;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ChunkSpliterator extends Spliterators.AbstractSpliterator<List<String>> {
    private final Spliterator<String> source;
    private final Predicate<String> delimiter;
    private final Consumer<String> getChunk;
    private List<String> current;

    ChunkSpliterator(Spliterator<String> lineSpliterator, Predicate<String> mark) {
        super(lineSpliterator.estimateSize(), ORDERED | NONNULL);
        source = lineSpliterator;
        delimiter = mark;
        getChunk = s -> {
            if (current == null) current = new ArrayList<>();
            current.add(s);
        };
    }

    public List<String> getCurrentChunk(){
        return current;
    }

    public Consumer<String> getChunkConsumer(){
        return getChunk;
    }


    public static Stream<List<String>> toChunks(
        Stream<String> lines, Predicate<String> splitAt, boolean parallel) {
        return StreamSupport.stream(
            new ChunkSpliterator(lines.spliterator(), splitAt),
            parallel);
    }

    public boolean tryAdvance(Consumer<? super List<String>> action) {
        while (current == null || !delimiter.test(current.get(current.size() - 1)))
            if (!source.tryAdvance(getChunk)) return lastChunk(action);
        current.remove(current.size() - 1);
        action.accept(current);
        current = null;
        return true;
    }

    private boolean lastChunk(Consumer<? super List<String>> action) {
        if (current == null) return false;
        action.accept(current);
        current = null;
        return true;
    }


}