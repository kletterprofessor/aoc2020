package com.bmw.aoc.year2020.Day04;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Spliterator;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


/**
 * @link https://stackoverflow.com/questions/39952739/reading-chunks-of-a-text-file-with-a-java-8-stream
 * @link https://stackoverflow.com/questions/26463771/how-to-process-chuncks-of-a-file-with-java-util-stream/26465398#26465398
 * @link https://stackoverflow.com/questions/3012788/how-to-check-if-a-line-is-blank-using-regex
 */
public class Day04WRO {

    /*
     * AoC Day 04 / Part 1 / Userstory 1
     * ---------------------------------
     *
     * As a programmer for AoC 2020 / Day 4 / Part 01 I like to have
     *  a function which extracts the records in the given file
     *  into instances of class PassportCredential
     *
     * Thus I need to split the file in to chunks which are separated by blank lines.
     * Then I need to parse the chunks
     */

    public static void main(String... args) {

        Path path = Paths.get("puzzles/2020/day04WRO.txt");

        Path test_valid_path = Paths.get("puzzles/2020/day04_part2_valid_passports.txt");
        Path test_invalid_path = Paths.get("puzzles/2020/day04_part2_invalid_passports.txt");

        // System.out.println("Result of part 1 is: " + part_1(path));
        System.out.println("Result of part 2 test valid is: " + part_2(test_valid_path));

        System.out.println("Result of part 2 test invalid is: " + part_2(test_invalid_path));

        System.out.println("Result of part 2 test: " + part_2(path));

    }

    public static long part_2(Path path){

        try (Stream<String> lines = Files.lines(path);) {

            Spliterator<Passport> passportSpliterator = new PassportSpliterator(lines);

            Stream<Passport> passportStream = StreamSupport
                .stream(passportSpliterator, false);

            Predicate<Passport> validPassportCheck1 = passport ->
                Pattern.compile("byr").asPredicate()
                    .and(Pattern.compile("iyr").asPredicate())
                    .and(Pattern.compile("eyr").asPredicate())
                    .and(Pattern.compile("hgt").asPredicate())
                    .and(Pattern.compile("hcl").asPredicate())
                    .and(Pattern.compile("ecl").asPredicate())
                    .and(Pattern.compile("pid").asPredicate())
                    .test(passport.getRawFlatPasswordData());

            return passportStream
                .filter(validPassportCheck1)
                .filter(passport -> passport.getBirthYear() >= 1920 && passport.getBirthYear() <= 2002)
                .filter(passport -> passport.getIssueYear() >= 2010 && passport.getIssueYear() <= 2020)
                .filter(passport -> passport.getExpirationYear() >= 2020 && passport.getExpirationYear() <= 2030)
                .filter(new Predicate<Passport>() {
                    @Override
                    public boolean test(Passport passport) {
                        if(passport.getMeasurementUnitOfHeight().equals("in"))
                            return passport.getHeightAsInt() >= 59 && passport.getHeightAsInt() <= 76;
                        else if (passport.getMeasurementUnitOfHeight().equals("cm"))
                            return passport.getHeightAsInt() >= 150 && passport.getHeightAsInt() <= 193;
                        else
                            return false;
                    }
                })
                .filter(passport -> Pattern.compile("#[0-9a-f]{6}").asPredicate().test(passport.getHairColor()))
                .filter(passport -> Pattern.compile("(amb|blu|brn|gry|grn|hzl|oth)").asPredicate().test(passport.getEyeColor()))
                .filter(passport -> Pattern.compile("^[0-9]{9}$").asPredicate().test(passport.getPassportID()))
                .peek(passport -> System.out.println(passport.getRawFlatPasswordData()))
                .count();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return -1;
    }

    public static long part_1(Path path){

        try (Stream<String> lines = Files.lines(path);) {

            Spliterator<Passport> passportSpliterator = new PassportSpliterator(lines);
            Stream<Passport> passportStream = StreamSupport.stream(passportSpliterator, false);

            Predicate<Passport> validPassportCheck2 = passport ->
                Pattern.compile("byr").asPredicate()
                    .and(Pattern.compile("iyr").asPredicate())
                    .and(Pattern.compile("eyr").asPredicate())
                    .and(Pattern.compile("hgt").asPredicate())
                    .and(Pattern.compile("hcl").asPredicate())
                    .and(Pattern.compile("ecl").asPredicate())
                    .and(Pattern.compile("pid").asPredicate())
                    .test(passport.getRawFlatPasswordData());

           return passportStream
                .filter(validPassportCheck2)
                .count();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return -1;
    }
}


