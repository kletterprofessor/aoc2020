package com.bmw.aoc.year2020.Day04;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Passport {

    final Pattern heightMeasurementPattern = Pattern.compile("(\\d*)(cm|in)");

    final private String rawFlatPasswordData;

    final private Map<String, String> passPortDataMap;

    public Passport(String rawFlatPasswordData){

        this.rawFlatPasswordData = rawFlatPasswordData;
        this.passPortDataMap = new HashMap<>();

        Arrays
            .asList(rawFlatPasswordData.split(" "))
            .stream()
            .forEach(s -> passPortDataMap.putIfAbsent(s.split(":")[0], s.split(":")[1]));

    }

    public String getRawFlatPasswordData() {
        return rawFlatPasswordData;
    }

    public Integer getBirthYear(){
        return Integer.parseInt(passPortDataMap.getOrDefault("byr", "0"));
    }

    public Integer getIssueYear(){
        return Integer.parseInt(passPortDataMap.getOrDefault("iyr", "0"));
    }

    public Integer getExpirationYear(){
        return Integer.parseInt(passPortDataMap.getOrDefault("eyr", "0"));
    }

    public String getHeightAsString(){

        return passPortDataMap.getOrDefault("hgt", "0cm");
    }

    public Integer getHeightAsInt(){

        Matcher matcher = heightMeasurementPattern.matcher(getHeightAsString());

        if(matcher.matches()){
            return Integer.parseInt(matcher.group(1));
        }
        return -1;
    }

    public String getMeasurementUnitOfHeight(){

        Matcher matcher = heightMeasurementPattern.matcher(getHeightAsString());

        if(matcher.matches()){
            return matcher.group(2);
        }
        return "invalid";
    }

    public String getHairColor(){

        return passPortDataMap.getOrDefault("hcl", "");
    }
    public String getEyeColor(){

        return passPortDataMap.getOrDefault("ecl", "");
    }

    public String getPassportID(){

        return passPortDataMap.getOrDefault("pid", "");
    }

    @Override
    public String toString() {
        return "Passport{" +
            "Birth Year = " + getBirthYear() + " " +
            "Issue Year = " + getIssueYear() + " " +
            "Expiration Year = " + getExpirationYear() + " " +
            "Height = " + getHeightAsString() + " " +
            "Height as int = " + getHeightAsInt() + " " +
            "Measurement Unit height " + getMeasurementUnitOfHeight() + " " +
            "Hair color: " + getHairColor() + " " +
            "Eye color: " + getEyeColor() + " " +
            "Passport ID: " + getPassportID() + " " +
            '}';
    }
}
