package com.bmw.aoc.year2020.Day04;

import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class PassportSpliterator implements Spliterator<Passport> {

    private final ChunkSpliterator chunkSplitIterator;

    private final Consumer<List<String>> getRawPassportRecordData;

    private List<String> currentRawPasswordData;

    private String flatRawPasswordData;


    public PassportSpliterator(Stream<String> lines) {

        Spliterator<String> lineSpliterator = lines.spliterator();
        Predicate<String> passportRecordDelimiter = Pattern.compile("^\\s*$").asPredicate();
        this.chunkSplitIterator = new ChunkSpliterator(lineSpliterator, passportRecordDelimiter);

        getRawPassportRecordData = strings -> {
                currentRawPasswordData = strings;
                flatRawPasswordData = String.join(" ", strings);
        };

    }

    @Override
    public boolean tryAdvance(Consumer<? super Passport> action) {

        boolean ret = chunkSplitIterator.tryAdvance(getRawPassportRecordData);

        if (ret) {
            Passport passport = new Passport(flatRawPasswordData);
            action.accept(passport);
        }

        return ret;
    }

    @Override
    public Spliterator<Passport> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return chunkSplitIterator.estimateSize();
    }

    @Override
    public int characteristics() {
        return chunkSplitIterator.characteristics();
    }
}
