package com.bmw.aoc.year2020;

import java.util.List;

import com.bmw.aoc.Utils;

public class Day02_MLEI2 {

	public static void main(String[] args) throws Exception {
		Day02_MLEI2 kata = new Day02_MLEI2();
		List<String> input = Utils.readInput(2, 2020);
		System.out.println(part1Basic(input));
		System.out.println(part2Basic(input));
	}

	public static String part1Basic(List<String> instructions) {

		String a;
		int valid = 0;

		for (int i = 0; i < instructions.size(); i++) {

			a = instructions.get(i);

			String[] list = a.split(" ");

			String[] listNumbers = list[0].split("-");
			String min = listNumbers[0];
			String max = listNumbers[1];

			String letter = list[1].substring(0, 1);

			String password = list[2];

			int count = 0;
			for (int x = 0; x < password.length(); x++) {

				if (password.charAt(x) == letter.charAt(0)) {
					count++;
				}
			}

			if (Integer.parseInt(min) <= count && count <= Integer.parseInt(max)) {
				valid++;
			}

		}
		System.out.println("valid Part 1: " + valid);

		return "done1";
	}

	public static String part2Basic(List<String> instructions) {

		String a;
		int count = 0;
		int zaehler = 0;

		for (int i = 0; i < instructions.size(); i++) {

			// System.out.println(a);
			String[] list = instructions.get(i).split(" ");

			String[] listNumbers = list[0].split("-");
			String positionFirst = listNumbers[0];
			String positionSecond = listNumbers[1];

			char letter = list[1].charAt(0);

			String password = list[2];

			int first = Integer.parseInt(positionFirst);
			int second = Integer.parseInt(positionSecond);

			if (((password.charAt(first - 1) == letter) ^ (password.charAt(second - 1) == letter))) {
				count++;
			}

		}

		System.out.println("valid Part 2: " + count + "; error: " + zaehler);

		return "done2";
	}

}
