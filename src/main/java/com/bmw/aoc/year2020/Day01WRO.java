package com.bmw.aoc.year2020;

import java.util.*;
import java.util.stream.Collectors;


import com.bmw.aoc.Utils;

public class Day01WRO {

    public static void main(String[] args) {

        System.out.println("AoC 20120 Day 1");

        List<String> input = Utils.readInput(1, 2020);

        var expenseReport = input.stream().map(x -> Integer.parseInt(x)).collect(Collectors.toList());

        List<TupelOfTwoValues> pairs =
            expenseReport.stream()
                .flatMap(i -> expenseReport.stream()
                    .map(j -> new TupelOfTwoValues(i,j))
                )
                .filter(x -> x.sumValues() == 2020)
                .collect(Collectors.toList());

        System.out.println(pairs);

        List<Tupel> list =
        expenseReport
            .stream()
            .flatMap(i -> expenseReport.stream()
                .flatMap(j -> expenseReport.stream()
                    .map(h -> new Tupel(i, j, h))
                )
            )
            .filter(x -> x.sumValues() == 2020)
            .collect(Collectors.toList());

        System.out.println(list);

    }

    static class Tupel {

        Integer firstValue;
        Integer secondValue;
        Integer thirdValue;

        Tupel(Integer firstValue, Integer secondValue, Integer thirdValue){
            this.firstValue = firstValue;
            this.secondValue = secondValue;
            this.thirdValue = thirdValue;
        }

        int sumValues(){
            return firstValue + secondValue + thirdValue;
        }

        @Override
        public String toString() {
            return "First value: " + firstValue.toString() + " Second Value: " + secondValue.toString() + " thirdValue: " + thirdValue.toString();
        }
    }

    static class TupelOfTwoValues {

        Integer firstValue;
        Integer secondValue;

        TupelOfTwoValues(Integer firstValue, Integer secondValue){
            this.firstValue = firstValue;
            this.secondValue = secondValue;
        }

        int sumValues(){
            return firstValue + secondValue;
        }

        @Override
        public String toString() {
            return "First value: " + firstValue.toString() + " Second Value: " + secondValue.toString();
        }
    }
}
