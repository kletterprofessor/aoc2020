package com.bmw.aoc.year2020;

import java.util.List;

import com.bmw.aoc.Utils;

public class Day03_MLEI {

	public static void main(String[] args) throws Exception {
		Day03_MLEI kata = new Day03_MLEI();
		List<String> input = Utils.readInput(3, 2020);
		System.out.println(part1Basic(input));
		System.out.println(part2Basic(input));
	}

	static char tree = '#';
	char ground = '.';

	public static int part1Basic(List<String> instructions) {

		int result = countTrees(instructions, 3, 1);

		return result;

	}

	public static long part2Basic(List<String> instructions) {
		long result = 0;

		long result1 = countTrees(instructions, 1, 1);
		long result2 = countTrees(instructions, 3, 1);
		long result3 = countTrees(instructions, 5, 1);
		long result4 = countTrees(instructions, 7, 1);
		long result5 = countTrees(instructions, 1, 2);

		result = result1 * result2 * result3 * result4 * result5;

		return result;
	}

	public static int countTrees(List<String> instructions, int right, int down) {
		int row = 0;
		int column = 0;
		int result = 0;

		while (row + down < instructions.size()) {

			row = row + down;
			column = column + right;

			while (column >= instructions.get(row).length()) {
				column = column - instructions.get(row).length();
			}

			char position = instructions.get(row).charAt(column);

			if (position == tree) {
				result++;
			}

		}
		return result;

	}
}