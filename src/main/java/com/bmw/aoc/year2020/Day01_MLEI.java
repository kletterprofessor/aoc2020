package com.bmw.aoc.year2020;

import java.util.List;
import java.util.stream.Collectors;

import com.bmw.aoc.Utils;

public class Day01_MLEI {

	public static void main(String[] args) throws Exception {
		Day01_MLEI kata = new Day01_MLEI();
		List<String> input = Utils.readInput(1, 2020);
		System.out.println(part1Basic(input));
		System.out.println(part1(input));
		System.out.println(part2Basic(input));
		System.out.println(part2(input));
	}

	public static String part1Basic(List<String> instructions) {
		int result = 0;

		for (int i = 0; i < instructions.size(); i++) {

			int a = Integer.parseInt(instructions.get(i));

			for (int zaehler = 0; zaehler < instructions.size(); zaehler++) {

				int b = Integer.parseInt(instructions.get(zaehler));

				if (a + b == 2020) {
					result = a * b;
				}

			}

		}
		return "" + result;

	}

	public static String part1(List<String> instructions) {
		int result = 0;

		List<Integer> list = instructions.stream().map(x -> Integer.parseInt(x)).collect(Collectors.toList());

		System.out.println(list);

		List<Multiplication> pairList = list.stream().flatMap(i -> list.stream().map(j -> new Multiplication(i, j)))
				.distinct().filter(x -> x.sumValues() == 2020).collect(Collectors.toList());
		System.out.println(pairList);

		return "Part1: " + result;

	}

	public static String part2Basic(List<String> instructions) {

		int result = 0;

		for (int i = 0; i < instructions.size(); i++) {

			int a = Integer.parseInt(instructions.get(i));

			for (int zaehler = 0; zaehler < instructions.size(); zaehler++) {

				int b = Integer.parseInt(instructions.get(zaehler));

				for (int z = 0; z < instructions.size(); z++) {

					int c = Integer.parseInt(instructions.get(z));

					if (a + b + c == 2020) {
						result = a * b * c;
					}
				}

			}

		}

		return "" + result;
	}

	public static String part2(List<String> instructions) {
		int result = 0;

		return "Part2: " + result;

	}

	public static Integer calculateResult(String a, String b) {

		return Integer.parseInt(a) * Integer.parseInt(b);
	}

	static class Multiplication {

		int first;
		int second;
		int third;

		public Multiplication(Integer first, Integer second) {
			this.first = first;
			this.second = second;
		}

		public int sumValues() {
			return first + second + third;
		}

		public Multiplication(Integer first, Integer second, Integer third) {
			this.first = first;
			this.second = second;
			this.third = third;
		}

	}
}
