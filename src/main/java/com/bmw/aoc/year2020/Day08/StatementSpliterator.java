package com.bmw.aoc.year2020.Day08;

import com.bmw.aoc.year2020.Day07.BagSpliterator;

import java.util.Scanner;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class StatementSpliterator implements Spliterator<Statement> {

    private static final Logger LOGGER = Logger.getLogger(StatementSpliterator.class.getName());

    private final Spliterator<String> lineSpliterator;

    public StatementSpliterator(Stream<String> input) {
        this.lineSpliterator = input.spliterator();
    }

    @Override
    public boolean tryAdvance(Consumer<? super Statement> action) {

        final String pattern = "(\\w+)\\s(.)(\\d+)";

        if(this.lineSpliterator.tryAdvance(line -> {

            Scanner scanner = new Scanner(line);

            scanner.findAll(pattern).forEach(matchResult -> {

                // get instruction from source
                String instruction = matchResult.group(1);
                LOGGER.fine("Instruction: " + instruction);

                Statement.Instruction instructionOfStatement = null;

                if(Statement.Instruction.NOP.toString().toLowerCase().equals(instruction))
                    instructionOfStatement = Statement.Instruction.NOP;
                else if(Statement.Instruction.ACC.toString().toLowerCase().equals(instruction))
                    instructionOfStatement = Statement.Instruction.ACC;
                else if(Statement.Instruction.JMP.toString().toLowerCase().equals(instruction))
                    instructionOfStatement = Statement.Instruction.JMP;
                else
                    throw new IllegalArgumentException("No valid instruction found");

                // Toggling SignFlag
                String signOfOperation = matchResult.group(2);
                LOGGER.fine("signOfOperation : " + signOfOperation);

                Boolean togglingSignFlac;

                if("+".equals(signOfOperation))
                    togglingSignFlac = Boolean.TRUE;
                else if("-".equals(signOfOperation))
                    togglingSignFlac = Boolean.FALSE;
                else
                    throw new IllegalArgumentException("No valid sign fond");

                // Value
                Integer value = Integer.parseInt(matchResult.group(3));
                LOGGER.fine("Value: " + value);

                Statement statement = new Statement(instructionOfStatement, togglingSignFlac, value);
                LOGGER.fine("statement: " + statement);

                action.accept(statement);

            });
        }))
            return true;
        else
            return false;

    }

    @Override
    public Spliterator<Statement> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return lineSpliterator.estimateSize();
    }

    @Override
    public int characteristics() {
        return lineSpliterator.characteristics();
    }
}
