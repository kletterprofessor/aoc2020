package com.bmw.aoc.year2020.Day08;

import com.bmw.aoc.year2020.Day07.Day07WRO;
import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;
import org.jgrapht.Graph;
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.alg.interfaces.StrongConnectivityAlgorithm;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.nio.dot.DOTExporter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Spliterator;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/*
Part 1: 1941
Part 2: 2096
 */

public class Day08WRO {

    private static final Logger LOGGER = Logger.getLogger(Day07WRO.class.getName());

    static public LogManager manager = LogManager.getLogManager();

    public static void main(String[] args) {

        try {

            InputStream stream = Day07WRO.class.getClassLoader().
                getResourceAsStream("logging.properties");

            manager.readConfiguration(stream);


        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }

        Path path = Paths.get("puzzles/2020/day08WRO.txt");


        Stream<String> lines = null;

        try {
            lines = Files.lines(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Spliterator<Statement> statementSpliterator = new StatementSpliterator(lines);
        Stream<Statement> statementStream = StreamSupport.stream(statementSpliterator, false);

        Long numberOfStatements = statementStream.count();
        System.out.println("numberOfStatements = " + numberOfStatements);

        Graph<String, DefaultEdge> directedGraph =
            new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);

        // Create graph with nodes
        Statement.getStatements().forEach(statement -> {
            directedGraph.addVertex(statement.getAddress().toString());
        });

        // Add edges
        Statement.getStatements().forEach(statement -> {
            if(statement.getAddressOfNextStatementRelativeToThisStatement() != -1)
                directedGraph.addEdge( statement.getAddress().toString(), statement.getAddressOfNextStatementRelativeToThisStatement().toString());
        });

        System.out.println("directedGraph = " + directedGraph);


        // computes all the strongly connected components of the directed graph
        StrongConnectivityAlgorithm<String, DefaultEdge> scAlg =
            new KosarajuStrongConnectivityInspector<>(directedGraph);

        List<Graph<String, DefaultEdge>> stronglyConnectedSubgraphs =
            scAlg.getStronglyConnectedComponents();

        // prints the strongly connected components
        System.out.println("Strongly connected components:");
        for (int i = 0; i < stronglyConnectedSubgraphs.size(); i++) {
            System.out.println(stronglyConnectedSubgraphs.get(i));
        }


        // Print out only ids of nodes
        // DOTExporter dotExporter = new DOTExporter();

        DOTExporter dotExporter = new DOTExporter();

        try {
            FileWriter fileWriter = new FileWriter("graph.dot");
            dotExporter.exportGraph(directedGraph, fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static void part02(Path path){


        Stream<String> lines = null;

        try {
            lines = Files.lines(path);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Spliterator<Statement> statementSpliterator = new StatementSpliterator(lines);
        Stream<Statement> statementStream = StreamSupport.stream(statementSpliterator, false);

        Long numberOfStatements = statementStream.count();
        System.out.println("numberOfStatements = " + numberOfStatements);

        Digraph digraphBeforeModification = createDiGraph(Statement.getStatements());
        DirectedCycle directedCycleBeforeModifications = new DirectedCycle(digraphBeforeModification);

        System.out.println("directedCycleBeforeModifications.hasCycle = " + directedCycleBeforeModifications.hasCycle());



        directedCycleBeforeModifications.cycle().forEach(integer -> System.out.println("statement in cycle: " + Statement.getStatements().get(integer)));


        BreadthFirstDirectedPaths pathConnectedToStart = new BreadthFirstDirectedPaths(digraphBeforeModification, 0);

        Statement.getStatements()
            .stream()
            .forEach(statement -> System.out.println("statement = " + statement + " dist:" + pathConnectedToStart.distTo(statement.getAddress())));


        Stream<Statement> streamOfStatements = StreamSupport.stream(Statement.getStatements().spliterator(), false);

        streamOfStatements.forEach(statement -> {

            System.out.println("statement = " + statement);

            Statement.Instruction instructionBeforeModification =
                statement.instruction;

            if("NOP".equals(statement.instruction.toString().toUpperCase())){
                statement.instruction = Statement.Instruction.JMP;
            }
            else if("JMP".equals(statement.instruction.toString().toUpperCase())){
                statement.instruction = Statement.Instruction.NOP;
            }

            System.out.println("mod.stmnt = " + Statement.getStatements().get(statement.address));

            Digraph digraphAfterModification = createDiGraph(Statement.getStatements());
            DirectedCycle directedCycleAfterModification =  new DirectedCycle(digraphAfterModification);

            Boolean hasCycleAfterModification = directedCycleAfterModification.hasCycle();
            System.out.println("hasCycleAfterModification = " + hasCycleAfterModification);

            statement.instruction = instructionBeforeModification;

        });



    }


    static public Integer part01(Path path){

        try {

            Stream<String> lines = Files.lines(path);
            Spliterator<Statement> statementSpliterator = new StatementSpliterator(lines);
            Stream<Statement> statementStream = StreamSupport.stream(statementSpliterator, false);

            var numberOfStatements = statementStream.count();

            Digraph programInstructionDiGraph = createDiGraph(Statement.getStatements());

            BreadthFirstDirectedPaths breadthFirstDirectedPaths = new BreadthFirstDirectedPaths(programInstructionDiGraph, 0);

            Statement
                .getStatements()
                .stream()
                .filter(statement -> breadthFirstDirectedPaths.hasPathTo(statement.getAddress()))
                .filter(statement -> "ACC".equals(statement.instruction.toString()))
                .forEach(statement -> {
                    if (statement.togglingNegativeOrPositiveOperation) {
                        Statement.accumulator += statement.value;
                    } else {
                        Statement.accumulator -= statement.value;
                    }
                });

        } catch (IOException e) {
            e.printStackTrace();
        }

        return Statement.accumulator;

    }

    static public Digraph createDiGraph(Collection<Statement> statements){

        Digraph programInstructionDiGraph = new Digraph(statements.size());

        statements.forEach(statement -> {
            if(statement.getAddressOfNextStatementRelativeToThisStatement() > 0){
                programInstructionDiGraph.addEdge(
                    statement.getAddress(),
                    statement.getAddressOfNextStatementRelativeToThisStatement()
                );

            }
        });

        return programInstructionDiGraph;

    }

}

