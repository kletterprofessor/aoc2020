package com.bmw.aoc.year2020.Day08;

import java.util.ArrayList;
import java.util.List;

public class Statement  {

    static List<Statement> statements = new ArrayList<>();

    static int accumulator = 0;

    static int statementPointer = 0;

    public enum Instruction {
        NOP, ACC, JMP
    }

    int address = -1;

    //  true    --> +
    //  false   --> -
    boolean togglingNegativeOrPositiveOperation = true;

    Instruction instruction = null;

    int value = -1;

    int numberWithinExecutionPlan = -1;

    public static List<Statement> getStatements() { return statements;};

    public Statement(final Instruction instruction, final Boolean togglingNegativeOrPositiveOperation, final Integer value) {
        this.address = statementPointer;

        this.instruction = instruction;
        this.togglingNegativeOrPositiveOperation = togglingNegativeOrPositiveOperation;
        this.value = value;

        statements.add(this);

        statementPointer += 1;
    }

    public Integer getAddressOfNextStatementRelativeToThisStatement(){

        Integer nextAddress = Integer.MAX_VALUE;

        if( instruction == Instruction.JMP){
            if(this.togglingNegativeOrPositiveOperation){
                nextAddress =  this.address + value;
            }
            else
                nextAddress=   this.address - value;
        }
        // check for last statement of program
        else if (this.address != statements.size() - 1)
            nextAddress =  this.address + 1;

        // sanity check if address is beyond program
        if(nextAddress >= statements.size())
            nextAddress =  -1 ;

        return nextAddress;

    }

    public Integer getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "Statement{" +
            "address=" + address +
            ", togglingNegativeOrPositiveOperation=" + togglingNegativeOrPositiveOperation +
            ", instruction=" + instruction +
            ", value=" + value +
            ", nextStatement= " + getAddressOfNextStatementRelativeToThisStatement() +
            '}';
    }


}
