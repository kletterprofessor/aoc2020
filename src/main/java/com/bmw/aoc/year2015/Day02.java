package com.bmw.aoc.year2015;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.bmw.aoc.Utils;

public class Day02 {

	public static void main(String[] args) throws Exception {
		List<String> input = Utils.readInput(2, 2015);
		System.out.println(part1(input));

		System.out.println(part2(input));
	}

	public static int calculateArea(List<Integer> dimensions) {

		int l = dimensions.get(0);
		int w = dimensions.get(1);
		int h = dimensions.get(2);

		int areaLW = l * w; // (2*4) = 8
		int areaWH = w * h; // (4*6) = 24
		int areaHL = h * l; // (6*2) = 12

		int min = Math.min(areaLW, areaWH);
		int minArea = Math.min(min, areaHL);

		return 2 * (areaLW + areaWH + areaHL) + minArea; // 2 * (8+24+12) + 8 = 96
	}

	public static int part1(List<String> instructions) {

		int result = instructions.stream().map(box -> Day02.calculateArea(Day02.getDimensions(box)))
				.collect(Collectors.summingInt(Integer::intValue));

		return result;
	}

	public static List<Integer> getDimensions(String boxDimensions) {

		List<Integer> listDimensions = (List<Integer>) Arrays.stream(boxDimensions.split("x"))
				.map(d -> Integer.parseInt(d)).collect(Collectors.toList());

		return listDimensions;
	}

	public static int part2(List<String> instructions) {

		int result = instructions.stream().map(box -> Day02.calculateRibbonLength(Day02.getDimensions(box)))
				.collect(Collectors.summingInt(Integer::intValue));

		return result;
	}

	static int calculateRibbonLength(List<Integer> dimensions) {
		int l = dimensions.get(0);
		int w = dimensions.get(1);
		int h = dimensions.get(2);

		System.out.println(dimensions);
		Collections.sort(dimensions);
		System.out.println(dimensions);
		int ribbonLength = 2 * (dimensions.get(0) + dimensions.get(1));

		int ribbonBowLength = l * w * h;

		return ribbonLength + ribbonBowLength;
	}
}
