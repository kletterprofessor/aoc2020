package com.bmw.aoc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Utils {

	public static List<String> readInput(int day, int year) {

		String yearAsString = String.valueOf(year);
		String tempString = String.valueOf(day);
		String dayStringPrefix = "day0";

		String completeDayString = dayStringPrefix.concat(tempString).concat(".txt");

		System.out.println("Read data from year: " + yearAsString + " and datafile: " + completeDayString);

		Path p = Paths.get("puzzles", yearAsString, completeDayString);

		try {
			return Files.lines(p).collect(Collectors.toList());
		} catch (IOException e) {
			return null;
		}
	}

	public static List<String> readInputTest(int day, int year) {

		String yearAsString = String.valueOf(year);
		String tempString = String.valueOf(day);
		String dayStringPrefix = "day0";

		String completeDayString = dayStringPrefix.concat(tempString).concat("Test").concat(".txt");

		System.out.println("Read data from year: " + yearAsString + " and datafile: " + completeDayString);

		Path p = Paths.get("puzzles", yearAsString, completeDayString);

		try {
			return Files.lines(p).collect(Collectors.toList());
		} catch (IOException e) {
			return null;
		}
	}

	public static List<String> readInputWRO(int day, int year) {

		String yearAsString = String.valueOf(year);
		String tempString = String.valueOf(day);
		String dayStringPrefix = "day0";

		String completeDayString = dayStringPrefix.concat(tempString).concat("WRO.txt");

		System.out.println("Read data from year: " + yearAsString + " and datafile: " + completeDayString);

		Path p = Paths.get("puzzles", yearAsString, completeDayString);

		try {
			return Files.lines(p).collect(Collectors.toList());
		} catch (IOException e) {
			return null;
		}
	}

	public static List<String> findRegexGroups(Pattern pattern, String text) {
		Matcher matcher = pattern.matcher(text);
		List<String> groups = new ArrayList<>();

		while (matcher.find()) {
			for (int i = 1; i <= matcher.groupCount(); i++) {
				groups.add(matcher.group(i));
			}
		}
		return groups;
	}

}
