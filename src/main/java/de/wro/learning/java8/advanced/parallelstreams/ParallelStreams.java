package de.wro.learning.java8.advanced.parallelstreams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParallelStreams {

    public static void main(String[] args) {

        // System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "1");

        // 1. Narrow the number of threads for parallel processing
        Stream.iterate("+", s ->  s + "+")
            .parallel()
            .limit(6)
            .peek(ParallelStreams::accept)
            .forEach(System.out::println);
        
        // 2. Be aware ot non concurrent aware Java classes

        List<String> list = new ArrayList<>();

        Stream.iterate("+", s ->  s + "+")
            .parallel()
            .limit(1000)
            // .peek(ParallelStreams::accept)
            .forEach(s -> list.add(s));

        System.out.println("list.size() = " + list.size());

        // 3. Use a collector to be threadsafe

        List<String> collect =
            Stream.iterate("+", s ->  s + "+")
            .parallel()
            .limit(1000)
            // .peek(ParallelStreams::accept)
            .collect(Collectors.toList());

        System.out.println("collect.size() = " +  collect.size());

    }

    private static void accept(String s) {
        System.out.println(s + " processed in the thread "+
            Thread.currentThread().getName()) ;
    }
}
