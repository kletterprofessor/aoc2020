package de.wro.learning.java8.advanced.spliterator.boundary;

import de.wro.learning.java8.advanced.spliterator.control.PersonSpliterator;
import de.wro.learning.java8.advanced.spliterator.entity.Person;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Spliterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CreatingSpliterator {

    public static void main(String... args) {

        Path path = Paths.get("files/people.txt");

        try {

            Stream<String> lines = Files.lines(path);
            //lines.forEach(System.out::println);

            Spliterator<String> linesSpliterator = lines.spliterator();

            // Step 1 - Provide nothing
            // Spliterator<Person> personSpliterator = null;

            // Step 2 - Provide a person spliterator and pass line spliterator as parameter
            Spliterator<Person> personSpliterator = new PersonSpliterator(linesSpliterator);

            Stream<Person> people = StreamSupport.stream(personSpliterator, false);
            people.forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
