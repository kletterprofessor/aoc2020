package de.wro.learning.java8.advanced.spliterator.control;

import de.wro.learning.java8.advanced.spliterator.entity.Person;

import java.util.Spliterator;
import java.util.function.Consumer;

public class PersonSpliterator implements Spliterator<Person> {
    private final Spliterator<String> lineSpliterator;
    private String name;
    private int age;
    private String city;

    public PersonSpliterator(Spliterator<String> linesSpliterator) {
        this.lineSpliterator = linesSpliterator;
    }

    @Override
    /**
     * Step 6
     * The stream API calls this method and passes a consumer
     *
     * Returns true when more entities of person can be read from the file
     *
     * Trick: Add the return values of the results of the tryAdvance-Method
     *        of the line spliterator
     */
    public boolean tryAdvance(Consumer<? super Person> action) {

        // First line of the file contains the name
        if (this.lineSpliterator.tryAdvance(line -> this.name = line) &&
            // Second line contains age
            this.lineSpliterator.tryAdvance(line -> this.age = Integer.parseInt(line)) &&
            // Third line contains city
            this.lineSpliterator.tryAdvance(line -> this.city = line)){

            Person p = new Person(name, age, city);
            action.accept(p);
            return true;

        } else {
            return false;
        }
    }

    @Override
    /**
     * Step 5
     * No parallelism  --> return null
     */
    public Spliterator<Person> trySplit() {
        return null;
    }

    @Override
    /**
     * Step 4
     * As we know that 3 lines make a person we can divide the number which is returned by the
     * used line spliterator by 3
     */
    public long estimateSize() {
        return this.lineSpliterator.estimateSize() / 3;
    }

    @Override
    /**
     * Step 3
     * Just return characteristics of used line spliterator
     */
    public int characteristics() {
        return lineSpliterator.characteristics();
    }
}
