package de.wro.learning.java8.advanced.streampatterns;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class FlatMapExamples {

    public static void main(String... args) {

        try {

            // Step 1: Read chunks of files
            Stream<String> stream_1 =Files.lines(Paths.get("files/TomSawyer_01.txt"));
            Stream<String> stream_2 =Files.lines(Paths.get("files/TomSawyer_02.txt"));
            Stream<String> stream_3 =Files.lines(Paths.get("files/TomSawyer_03.txt"));
            Stream<String> stream_4 =Files.lines(Paths.get("files/TomSawyer_04.txt"));

//            System.out.println("Stream 1: " + stream_1.count());
//            System.out.println("Stream 2: " + stream_2.count());
//            System.out.println("Stream 3: " + stream_3.count());
//            System.out.println("Stream 4: " + stream_4.count());

            // Step 2: Concatenate the streams

            Stream<Stream<String>> streamOfStreams
                = Stream.of(stream_1, stream_2, stream_3, stream_4);

//            System.out.println("# of streams: " + streamOfStreams.count());

            // Step 3: flatMap into one big stream of String

//            Stream<String> streamOfLines =
//                streamOfStreams.flatMap(stream -> stream);
//
//            System.out.println("# lines: " + streamOfLines.count());

            // Alternative code
//            Stream<String> streamOfLines_2 = streamOfStreams.flatMap(Function.identity());
//
//            System.out.println("# lines: " + streamOfLines_2.count());

            // Step 4: Count the words in the concatenated stream
            Stream<String> streamOfLines_2 = streamOfStreams.flatMap(Function.identity());

            Function<String, Stream<String>> lineSplitter =
                line -> Pattern.compile(" ").splitAsStream(line);

            Stream<String> streamOfWords = streamOfLines_2.flatMap(lineSplitter);

            System.out.println("# words: " + streamOfWords.count());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
