package de.wro.learning.java8.lambda;

import java.util.function.Function;

/**
 *
 * @author José Paumard
 */
@FunctionalInterface
public interface Comparator<T>  {

    public int compare(T t1, T t2);

    /**
     *
     * @see Comparable
     * @see Comparable#compareTo(Object)
     *
     * Thus to use this method requires, that the interface Comparable is implemented the right
     * way, which is pretty much the case for all wrapper types String, Integer...
     */
    public static <U> Comparator<U> comparing(Function<U, Comparable> refMethodOfAnyTypeWhichHasImplementedComparableCompareTo) {

        return (p1, p2) ->
            refMethodOfAnyTypeWhichHasImplementedComparableCompareTo
                .apply(p1) // call of getter p1
                .compareTo(refMethodOfAnyTypeWhichHasImplementedComparableCompareTo // do comparison
                    .apply(p2)); // call of getter p2
    }

    public default Comparator<T> thenComparing(Comparator<T> cmp) {

        return (p1, p2) -> compare(p1, p2) == 0 ? cmp.compare(p1, p2) : compare(p1, p2) ;
    }


    public default Comparator<T> thenComparing(Function<T, Comparable> f) {

        return thenComparing(comparing(f)) ;
    }
    
    public default Comparator<T> thenComparingStep5(Comparator<T> nextComparator){
        return (t1, t2) -> compare(t1, t2)
            == 0 ? nextComparator.compare(t1, t2) : compare(t1,t2);
    }

    public default Comparator<T> thenComparingStep6(Function<T, Comparable> refMethodOfAnyTypeWhichHasImplementedComparableCompareTo){

        Comparator<T> cmp = comparing(refMethodOfAnyTypeWhichHasImplementedComparableCompareTo);
        return (thenComparingStep5(cmp));
    }


    /**
    **  Comparison method which only is able to compare methods which
    **  are returning Integer
     */
    public static Comparator<Person> comparingPersonIntegerAttribute(Function<Person, Integer> personIntegerAttribute){

        return (p1, p2) -> personIntegerAttribute.apply(p1) - personIntegerAttribute.apply(p2);
    }

    /**
     **  Comparison method which only is able to compare methods which
     **  are returning String
     */
    public static Comparator<Person> comparingPersonStringAttribute(Function<Person, String> personStringAttribute){

        return (p1, p2) -> -1;
    }


}

