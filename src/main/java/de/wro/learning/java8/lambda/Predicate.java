package de.wro.learning.java8.lambda;

@FunctionalInterface
public interface  Predicate<T> {

    public boolean test(T t);

    public default Predicate<T> and(Predicate<T> other){

        return t -> test(t) && other.test(t);
    }
}
