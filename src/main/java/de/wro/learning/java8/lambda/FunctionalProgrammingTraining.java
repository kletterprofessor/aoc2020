package de.wro.learning.java8.lambda;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class FunctionalProgrammingTraining {

    public static void main(String... args) {

        de.wro.learning.java8.lambda.Predicate<String> p1 = s -> s.length() <  20;
        de.wro.learning.java8.lambda.Predicate<String> p2 = s -> s.length() > 5;

        de.wro.learning.java8.lambda.Predicate p3 = p1.and(p2);

        System.out.println("P3 for Yes: " + p3.test("Yes"));
        System.out.println("P3 for Good morning : " + p3.test("Good morning"));
        System.out.println("P3 for Good morning gentleman: " + p3.test("Good morning gentleman"));

    }

    public void sampleOfFourCategoriesOfFunctionalInterfaces(){

        // Consumer sample
        Consumer<String> printer = s -> System.out.println(s);

        // Supplier sample

        Supplier<Person> personSupplier = () -> new Person();

        // !!! No parameters allowed !!!
        Supplier<Person> personSupplier2 = Person::new;

        // Function sample
        Function<Person, Integer> ageMapper = person -> person.getAge();

        // Predicate sample
        Predicate<Person> ageGT20 = person -> person.getAge() > 20;

    }

}
