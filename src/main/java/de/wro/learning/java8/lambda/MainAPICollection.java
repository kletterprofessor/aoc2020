package de.wro.learning.java8.lambda;

import java.util.*;
import java.util.Comparator;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class MainAPICollection {

    public static void main(String ... args) {


        SimplePerson p1 = new SimplePerson("Alice", 23);
        SimplePerson p2 = new SimplePerson("Brian", 56);
        SimplePerson p3 = new SimplePerson("Chelsea", 46);
        SimplePerson p4 = new SimplePerson("David", 28);
        SimplePerson p5 = new SimplePerson("Erica", 37);
        SimplePerson p6 = new SimplePerson("Francisco", 18);

        List<SimplePerson> people = new ArrayList<>(Arrays.asList(p1, p2, p3, p4, p5, p6));

        System.out.println("--- Build up list of persons ---");
        // Print with simple consumer
        people.forEach(simplePerson -> System.out.println(simplePerson));

        System.out.println("--- Remove people age < 30 ---");
        // Sample of removeIf
        people.removeIf(simplePerson -> simplePerson.getAge() < 30);

        // Print with method reference
        people.forEach(System.out::println);

        System.out.println("--- Replace with upper case ---");
        // Replace instances with new instance where name is uppercase
        people.replaceAll(
            simplePerson -> new SimplePerson(
                simplePerson.getName().toUpperCase(),
                simplePerson.getAge()));

        people.forEach(System.out::println);

        System.out.println("--- Sorting by ages ---");

        people.sort(Comparator.comparing(SimplePerson::getAge));

        people.forEach(System.out::println);

        System.out.println("--- Sorting by ages reverse ---");

        people.sort(Comparator.comparing(SimplePerson::getAge).reversed());

        people.forEach(System.out::println);

        System.out.println("--- Map interface new methods ---");

        City newYork = new City("New York");
        City shanghai = new City("Shanghai");
        City paris = new City("Paris");

        Map<City, List<SimplePerson>> map = new HashMap<>();

        // Old style
        System.out.println("People from Paris: " + map.get(paris));

        System.out.println("--- Map interface new methods : getOrDefault---");
        // New style JDK 1.8
        System.out.println("People from Paris: " + map.getOrDefault(paris, Collections.EMPTY_LIST));

        System.out.println("--- Map interface new methods: putIfAbsent ---");

        map.putIfAbsent(paris, new ArrayList<>());
        map.get(paris).add(p1);

        System.out.println("People from Paris: " + map.getOrDefault(paris, Collections.EMPTY_LIST));

        System.out.println("--- Map interface new methods: computeIfAbsent ---");
        // Function Input: T (ype) City, R (esult) : List<SimmplePerson>
        Function<City, List<SimplePerson>> function = city -> new ArrayList<>();

        map.computeIfAbsent(newYork, function).add(p2);

        // Function will not be executed here as the list is already present
        map.computeIfAbsent(newYork, function).add(p3);

        System.out.println("People from New York: " +
            map.getOrDefault(newYork, Collections.EMPTY_LIST));


        System.out.println("--- Map interface new methods : merge---");

        Map<City, List<SimplePerson>> map1 = new HashMap<>();
        map1.computeIfAbsent(newYork, city -> new ArrayList<>()).add(p1);
        map1.computeIfAbsent(shanghai, city -> new ArrayList<>()).add(p2);
        map1.computeIfAbsent(shanghai, city -> new ArrayList<>()).add(p3);

        System.out.println("Map 1");
        map1.forEach((city, peopleOfCity) -> System.out.println(city + " : " + peopleOfCity));


        Map<City, List<SimplePerson>> map2 = new HashMap<>();
        map2.computeIfAbsent(shanghai, city -> new ArrayList<>()).add(p4);
        map2.computeIfAbsent(paris, city -> new ArrayList<>()).add(p5);
        map2.computeIfAbsent(paris, city -> new ArrayList<>()).add(p6);

        System.out.println("Map 2");
        map2.forEach((city, peopleOfCity) -> System.out.println(city + " : " + peopleOfCity));

        // Add everything from second list into the first list and return the first list as result
        BiFunction<List<SimplePerson>, List<SimplePerson>, List<SimplePerson>>
            biFunctionToAddAllFromSecondListIntoFirstList =
            (simplePeople1, simplePeople2) -> {
                simplePeople1.addAll(simplePeople2);
                return simplePeople1;
            };

        // merge based on the key city the list of people into map 1
        BiConsumer<City, List<SimplePerson>> biConsumerToMergeBasedOnKeyCityAllPeopleIntoMap1
            = (city, simplePeople) -> map1.merge(
                city,
                simplePeople,
            biFunctionToAddAllFromSecondListIntoFirstList);

        map2.forEach(biConsumerToMergeBasedOnKeyCityAllPeopleIntoMap1);

        map1.forEach(((city, simplePeople) -> System.out.println("city: " + city  + "people: " + simplePeople)));

    }
}
