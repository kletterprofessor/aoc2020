package de.wro.learning.java8.lambda;

import java.util.*;
import java.util.Comparator;

public class DataProcessingUsingLambaAndCollectionSamples {


    public static void main(String ... args) {

        // Sample for collection iterator
        List<Person> people = new ArrayList<>();

        people.forEach(System.out::println);
        people.removeIf(person -> person.getAge() > 20);

        // Sample for list iterator
        List<String> names = new ArrayList<>();

        // Unary operator
        names.replaceAll(name-> name.toUpperCase());
        names.replaceAll(String::toUpperCase);

        // Comparator
        people.sort(
            Comparator
                .comparing(Person::getAge)
                .thenComparing(Person::getLastName));

        // Map
        Map<City, List<Person>> map = new HashMap<>();
        map.forEach(((city, people1) -> System.out.println( city + ": " + people.size() + " people")));

    }
}
