package de.wro.learning.java8.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;


public class LambdaProgrammingTraining {

    public static void main(String... args) {

        // Step 1 / Compare each attribute
        Comparator<Person> cmp = (p1, p2) -> p2.getAge() - p1.getAge();
        Comparator<Person> cmpFirstName = (p1, p2) -> p1.getFirstName().compareTo(p2.getFirstName());
        Comparator<Person> cmpLastName = (p1, p2) -> p1.getLastName().compareTo(p2.getLastName());

        // Step 2 / Use a function for comparison
        Function<Person, Integer> functionGetAgeOfPerson = p -> p.getAge();
        Function<Person, String> functionGetFirstNameOfPerson = p -> p.getFirstName();
        Function<Person, String> functionGetLastNameOfPerson= p -> p.getLastName();

        // Step 3 / Refactoring : Build a static method for comparing Attributes Comparator::comparingPersonAttributes
        // and use method references of class Person
        Comparator<Person> personComparator1 = Comparator.comparingPersonIntegerAttribute(functionGetAgeOfPerson);
        Comparator<Person> personComparator2 = Comparator.comparingPersonIntegerAttribute(p -> p.getAge());
        Comparator<Person> personComparator3 = Comparator.comparingPersonIntegerAttribute(Person::getAge);

        // Still Step 3 / Proving comparator for string attributes as well
        Comparator<Person> personComparator4 = Comparator.comparingPersonStringAttribute(Person::getFirstName);

        // Step 4 / Using generic comparing for any type which has implemented Comparable::compareTo correctly
        Comparator<Person> personComparator5 = Comparator.comparing(Person::getFirstName);

        // Step 5 / Chaining comparators
        Comparator<Person> cmpAgeStep5 = Comparator.comparing(Person::getAge);
        Comparator<Person> cmpLastNameStep5 = Comparator.comparing(Person::getLastName);
        Comparator<Person> cmpChaining = cmpAgeStep5.thenComparingStep5(cmpLastNameStep5);

        // Step 6 / Chaining method references
        Comparator<Person> cmpStep6=
            Comparator
                .comparing(Person::getAge)
                .thenComparingStep6(Person::getFirstName);

        // Final Summary Module 1
        Comparator cmpPersonChained =
                Comparator.comparing(Person::getLastName)
                .thenComparing(Person::getFirstName)
                .thenComparing(Person::getAge);
    }

    public static void basicLambdaProgrammingSample(){

        // Play with comparing Strings

        String s1 = "Hello odd World";
        String s2 = "Hello World";
        String s3 = "Hello Wrld";

        List<String> stringArrayList = new ArrayList<>();

        stringArrayList.add(s1);
        stringArrayList.add(s2);
        stringArrayList.add(s3);

        System.out.println("Before sorting:" + stringArrayList);

        // Manually created comparator
        stringArrayList.sort((str1, str2) -> Integer.compare(str1.length(), str2.length()));

        // Build-in comparator of class String
        stringArrayList.sort(String::compareTo);

        System.out.println("After sorting:" + stringArrayList);


    }

    Comparator<String> comparatorTraditional = new Comparator<String>() {
        public int compare(String s1, String s2) {
            return Integer.compare(s1.length(), s2.length());
        }
    };

    public Comparator<String> comparatorFunctional =
        (s1, s2) ->
            Integer.compare(s1.length(), s2.length());

    Runnable runnableTraditional = new Runnable() {
        public void run() {
            int i = 0;
            while (i++ < 10) {
                System.out.println("It works!");
            }
        }
    };

    Runnable runnableFunctional = () -> {
        int i = 0;
        while (i++ < 10) {
            System.out.println("It works!");
        }
    };

}

